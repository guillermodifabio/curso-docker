import {Employee} from './employee';

export class Department {

  id: string;
  name: string;
  employees: Employee[];

  constructor(o?: Partial<Department>) {
    Object.assign(this, o);
  }
}
