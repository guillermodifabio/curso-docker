import {Component, Input, OnInit, Predicate} from '@angular/core';
import {Department} from '../../models/department';
import {DepartmentService} from '../../services/department.service';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {Employee} from '../../models/employee';
import {EmployeeService} from '../../services/employee.service';
import {flatMap, switchMap, tap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog, MatSnackBar} from '@angular/material';
import {of} from 'rxjs';

function validateName(checker: boolean) {
  return (formControl => {
    if (!formControl.parent) {
      return null;
    }

    if (checker) {
      return Validators.required(formControl);
    }

    return null;
  });
}

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {

  departmentList: Department[];

  titulo: string;

  newDepartment = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    age: [null, [Validators.required]],
    position: [null, [Validators.required]],
    departmentSelected: [null, [Validators.required]],
    departmentName: [null, [
      validateName (this.newDepartment)]]
  });

  isSaving = false;

  constructor(public fb: FormBuilder,
              private service: DepartmentService,
              private employeeService: EmployeeService,
              private snackBar: MatSnackBar,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.service.getAll().subscribe(data => {
      this.departmentList = data;
    });

    this.route.params.pipe(
      switchMap((params) => {
        if (params['id']) {
          this.titulo = 'Editar Empleado';
          return this.employeeService.get(params['id']);
        }
        this.titulo = 'Nuevo Empleado';
        return of(null);
      })
    ).subscribe((employee) => {
      if (employee) {
        this.editForm.patchValue({
          id: employee.id,
          name: employee.name,
          age: employee.age,
          position: employee.position,
          departmentSelected: employee.department
        });
      }
    });


  }

  selectDepartment() {
    if (this.editForm.get('departmentSelected').value === 'new') {
      this.newDepartment = true;
    } else {
      this.newDepartment = false;
      this.editForm.get('departmentName').setValue(null);
    }
  }

  onSubmit() {

    this.isSaving = true;

    const employee = {
      ...new Employee(),
      name: this.editForm.get('name').value,
      age: this.editForm.get('age').value,
      position: this.editForm.get('position').value
    };

    const id = this.editForm.get('id').value;
    if (id != null) {
      employee.id = id;
    }

    if (this.newDepartment) {
      const dept = new Department();
      dept.name = this.editForm.get('departmentName').value;
      this.service.create(dept).pipe(
        tap(depto => employee.department = depto),
        flatMap(() => this.employeeService.create(employee))
      ).subscribe(
        () => this.saveSuccesfull(),
        (e) => this.saveError(e));
    } else {
      employee.department = this.editForm.get('departmentSelected').value;
      this.employeeService.create(employee).subscribe(
        () => this.saveSuccesfull(),
        (e) => this.saveError(e));
    }
  }

  saveSuccesfull() {
    this.snackBar.open('La operacion se realizo exitosamente', 'cerrar');
    this.router.navigate(['/departments']);
  }

  saveError(error: Error) {
    // console.log('Ocurrio un Error:');
    // console.log(error.message);
    console.log(location.host);
    this.snackBar.open('Ocurrio un error: ' + error.message, 'cerrar');
  }

  public get ctrls() {
    return this.editForm.controls;
  }

  public compareById(item1, item2): boolean {
    return item2 && item1 && item1.id === item2.id;
  }

}
