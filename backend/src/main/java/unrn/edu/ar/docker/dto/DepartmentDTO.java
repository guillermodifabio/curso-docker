package unrn.edu.ar.docker.dto;

import org.springframework.data.mongodb.core.mapping.Field;
import unrn.edu.ar.docker.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class DepartmentDTO {

    private String id;

    private String name;

    private List<EmployeeDTO> employees = new ArrayList<>();

    public DepartmentDTO() {

    }

    public DepartmentDTO(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeDTO> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeDTO> employees) {
        this.employees = employees;
    }
}
